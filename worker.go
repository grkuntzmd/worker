package main

import (
	"fmt"
	"math/big"
	"runtime"
	"sync"
	"time"
)

const (
	maxWorkers = 8
	runs       = 20
	maxNumber  = 20000
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	var sumTime float64
	for r := 0; r < runs; r++ {
		sumTime += sequential()
	}

	avg := sumTime / float64(runs)
	fmt.Printf("Sequential\t%f\n", avg)

	sumTime = 0
	for r := 0; r < runs; r++ {
		sumTime += goroutines()
	}

	avg = sumTime / float64(runs)
	fmt.Printf("Goroutines\t%f\n", avg)

	for w := 1; w <= maxWorkers; w++ {
		sumTime = 0
		for r := 0; r < runs; r++ {
			sumTime += workers(w)
		}

		avg := sumTime / float64(runs)
		fmt.Printf("%d\t%f\n", w, avg)
	}
}

// From https://rosettacode.org/wiki/Prime_decomposition#Go
var (
	ZERO = big.NewInt(0)
	ONE  = big.NewInt(1)
)

func Primes(n *big.Int) []*big.Int {
	res := []*big.Int{}
	mod, div := new(big.Int), new(big.Int)
	for i := big.NewInt(2); i.Cmp(n) != 1; {
		div.DivMod(n, i, mod)
		for mod.Cmp(ZERO) == 0 {
			res = append(res, new(big.Int).Set(i))
			n.Set(div)
			div.DivMod(n, i, mod)
		}
		i.Add(i, ONE)
	}
	return res
}

func goroutines() float64 {
	start := time.Now()

	var wg sync.WaitGroup

	for c := 2; c < maxNumber; c++ {
		wg.Add(1)
		go func(n int64) {
			defer wg.Done()
			_ = Primes(big.NewInt(n))
		}(int64(c))
	}

	wg.Wait()

	return time.Since(start).Seconds()
}

func sequential() float64 {
	start := time.Now()

	for c := 2; c < maxNumber; c++ {
		_ = Primes(big.NewInt(int64(c)))
	}

	return time.Since(start).Seconds()
}

func workers(w int) float64 {
	start := time.Now()

	var wg sync.WaitGroup
	wg.Add(w)

	in := make(chan *big.Int, maxNumber)

	for i := 0; i < w; i++ {
		go func() {
			defer wg.Done()

			for n := range in {
				_ = Primes(n)
			}
		}()
	}

	for c := 2; c < maxNumber; c++ {
		in <- big.NewInt(int64(c))
	}
	close(in)

	wg.Wait()

	return time.Since(start).Seconds()
}
