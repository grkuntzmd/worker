# Golang workers
This small Go program compares traditional go routine-based parallelization
to worker based parallelization.

In the worker pattern, a small number of parallel workers are started. They
read the work to be done from a channel. When the workers have exhausted
the channel, they exit.

The main thread creates the workers, then queues the work into the channel,
closes the channel, then waits for the workers to complete.